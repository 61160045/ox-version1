
import java.util.Scanner;
import java.io.IOException;

public class OX {

    static char table[][] = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';
    static char winner = '-';
    static int row;
    static int col;
    static boolean isFinish = false;

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            inputRowCol();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void switchPlayer() {
        if (player == ('O')) {
            player = 'X';
        } else if (player == ('X')) {
            player = 'O';
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }

    static boolean checkDraw(int countnum, char table[][]) {
        if (countnum == 9) {
            showTable();
            showDraw();
            return true;
        }
        return false;
    }

    static void showDraw() {
        System.out.println("Draw.");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not available.");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkObilque() {
        if (table[0][0] != player && table[1][1] != player && table[2][2] != player) {
            return;
        }
        if (table[0][0] != 'O' && table[1][1] != 'O' && table[2][2]
                != 'O') {
            return;
        }
        isFinish = true;
        winner = player;
    }

    static void checkDraw() {
        showTable();
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkObilque();
        checkDraw();
    }
}
